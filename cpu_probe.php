<?php 
/* CPU probe
 * @copyright Copyright (C) 2012 NetPlayTV
 * @author Henri Grech-Cini<henry-grechcini@netplaytv.com>
 * @date    28/02/2012
 */

$memcache = new Memcache(); 
$memcache->addServer('127.0.0.1', 11211); //put here the same host/port that the website uses 

$hostname = exec('hostname'); 

$loadavgraw = exec('cat /proc/loadavg'); 
$loadavgsplit = split(' ', $loadavgraw); 
$load = $loadavgsplit[0]; 

echo $hostname.' CPU load = ' . $load; 
$memcache->set($hostname.'-db-throttle-cpu-load', $load);
echo PHP_EOL;

echo $hostname.' Timestamp = ' . time(); 
$memcache->set($hostname.'-db-throttle-timestamp', time()); 
echo PHP_EOL;

