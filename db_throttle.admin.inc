<?php
/**
 * @file
 * Adminstrative pages for Database Throttle
 * 
 * @copyright Copyright (C) 2012 NetPlayTV
 * @author Marcelo Vani <marcelo.vani@netplaytv.com>
 * @author Henri Grech-Cini<henry-grechcini@netplaytv.com>
 * @date    28/02/2012
 *
 */

 /**
 * Configuration form
 * 
 * @return
 * The configuration form 
 */ 
function db_throttle_configuration_form() {
  $form['db_throttle'] = array(
    '#type' => 'fieldset',
    '#title' => t('Database Throttle'),
  );
  $form['db_throttle']['db_throttle_enable'] = array(
    '#type' => 'checkbox',
    '#title'         => t('Enable'),
    '#default_value' => variable_get('db_throttle_enable', 0),
  );
  return system_settings_form($form);  
}

