Database Throttle
------------------------
Drupal 6

Author: Marcelo Vani / Henri Grech-Cini
Copyright: NetplayTv

Overview:
--------
Temporarily disables access to databases when CPU load is too high.
This module is most suitable for non-essential databases. Throttling the main database might cause the site not to be displayed correctly.

Requirements:
------------
- Linux
- Memcache
- Crontabs

Features:
---------
- You can tick the databases you want to throttle
- You can specify the CPU threshold for each database

Installation:
-------------
- Install the module
- Configure database server
	- Install the probe script (cpu_probe.php) on /usr/local/bin/
	  Change the host/port to suit your setup
	
	- Configure a crontab to run the probe script. If you are not familiar with crontabs, you can have a look at 
	  this link http://kevin.vanzonneveld.net/techblog/article/schedule_tasks_on_linux_using_crontab/
	  
	  Crontab's minimum interval is one minute which sometimes can be too long until you take any action. 
	  If you need the script to be run in a shorter interval, you need to create a loop like it's suggested
	  on this link http://www.linuxquestions.org/questions/programming-9/seting-cron-to-run-a-script-every-second-425623/#post4105711
	     
	- Configure any firewall rules that might be necessary to allow memcache to communicate to memcache on the web servers
   
Configuration:
--------------
- Go to /admin/settings/db-throttle
- Tick the databases you want to enable throttle and set the threshold 

Testing
-------


Last updated:
------------
; $Id: README.txt,v 1.0 2012/02/28 12:13:12 mvani Exp $
